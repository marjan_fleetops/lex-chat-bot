const { jsWithTs: tsjPreset } = require('ts-jest/presets');

module.exports = {
  "roots": [
    "<rootDir>"
  ],
  "transform": {
    ...tsjPreset.transform,
  },
  "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.ts$",
  "moduleFileExtensions": [
    "ts",
    "js",
    "json",
    "node"
  ],
  "reporters": [
    "default",
    ["jest-junit", {"output": "./test-reports/junit.xml"}]
  ],
  "coveragePathIgnorePatterns": [
    "/node_modules/",
    "tsconfig.json",
    "mock.json",
    "package.json",
    "package-lock.json"
  ],
};
