import { Context, LexDialogActionClose, LexEvent, LexResult } from 'aws-lambda';
// import loggerCreate from '../../logging';

// const logger = loggerCreate('LexChatBot');

// export const dummyLoads = [
//   'Okay I have found load from Toronto, Ontario to New York for flat rate of US 600',
//   'Okay I have found load from Vancouver, British Columbia to San Francisco for flat rate of US 1,500',
// ];

export const flatRateGenerator = () => {
  const lowerLimit = 100;
  const upperLimit = 1000;
  return Math.floor((Math.random() * (upperLimit - lowerLimit)) + lowerLimit);
};

export const LexChatBotEntry = async function (event: LexEvent, context: Context | any) {
  const { currentIntent: { slots: { pickup_location, dropoff_location } }, sessionAttributes }: LexEvent = event;

  const message = {
    contentType: 'PlainText',
    content: `Okay, I have found load from ${pickup_location} to ${dropoff_location} for flat rate of ${flatRateGenerator()} USD`,
  } as LexDialogActionClose['message'];

  return {
    sessionAttributes,
    dialogAction: {
      type: 'Close',
      fulfillmentState: 'Fulfilled',
      message,
    },
  } as LexResult;
};
