import { LexChatBotEntry } from './handler';
import { LexDialogActionClose } from 'aws-lambda';
import mockEvent from './mock.json';


describe('LexChatBot', () => {

  let event;

  beforeEach(() => {
    event = mockEvent;
  });

  // test('return content contains one of the dummy responses', async () => {
  //   const { dialogAction } = await LexChatBotEntry(event, {});
  //   const { message } = dialogAction as LexDialogActionClose;
  //   if (message) {
  //     const { content } = message;
  //     const contentFound = dummyLoads.find(loadContent => loadContent === content);
  //     expect(contentFound).toBeTruthy();
  //   }
  // });

  test('return content contains non empty string', async () => {
    const { dialogAction } = await LexChatBotEntry(event, {});
    const { message } = dialogAction as LexDialogActionClose;
    if (message) {
      const { content } = message;
      expect(content).toBeTruthy();
    }
  });

});
