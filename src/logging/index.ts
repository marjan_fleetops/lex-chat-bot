import { createLogger, format, transports } from 'winston';

const WinstonGraylog2 = require('winston-graylog2');

const { combine, timestamp, printf } = format;

const customFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${level}]: ${message}`;
});

export default (lambdaName: string) => (
  createLogger({
    format: combine(
      timestamp(),
      customFormat,
    ),
    transports: [
      new transports.Console({ // for cloudwatch
        handleExceptions: true,
      }),
      new WinstonGraylog2({
        exitOnError: false,
        handleExceptions: true,
        graylog: {
          servers: [{ host: process.env.GRAYLOG_IP, port: process.env.GRAYLOG_PORT }],
          facility: lambdaName,
        },
      }),
    ],
  })
);
